from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class WebDriverCfg:
    def __init__(self, path):
        """This function is a constructor for WebDriver.
        """
        self.driver = webdriver.Chrome(path)
        self.driver.get("https://projectby.trainings.dlabanalytics.com/nlozgacheva/tree#notebooks")

    def click_on_element(self, element, find_by = By.LINK_TEXT):
        """This function simulates the ckick on the element on the page.
        """
        link = WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((find_by, element)))
        link.click()

    def send_key(self, element, key):
        """This function finds the element and write some cfg.
        """
        needed_element = self.driver.find_element_by_id(element)
        needed_element.send_keys(key)

    def switch_to_last_window(self):
        """This function switches to the last window.
        """
        self.driver.switch_to_window(self.driver.window_handles[-1])