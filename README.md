# automation_dqe

## HOWTO:

1. Prepare config file. Write you configuration: login and password to DLab.
2. Prepare python environment by "pip install -r requirements.txt".
3. Run automation for Data Quality rules by "python .\auto_api.py"