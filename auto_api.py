from webdrivercfg import WebDriverCfg
from selenium.webdriver.common.by import By
import time
import json

def run():
    """This function runs automation for Jupiter notebook.
    """
    with open('config.json') as f:
        data_config = eval(f.read())

    driver = WebDriverCfg(data_config["path"])
    driver.click_on_element("EPAM SSO")
    driver.send_key("userNameInput", data_config["login"])
    driver.send_key("passwordInput", data_config["password"])
    driver.click_on_element("submitButton", find_by = By.ID)
    driver.click_on_element("data_quality_checks.ipynb")
    time.sleep(2)
    driver.switch_to_last_window()
    for i in range(13):
        driver.click_on_element('//*[@title="run cell, select below"]', find_by = By.XPATH)
        time.sleep(1)


if __name__ == '__main__':
    run()
    time.sleep(100000)